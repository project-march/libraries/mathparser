import sys
import operator
from mathparser import parser as pp


operations = {
    pp.TokenType.T_PLUS: operator.add,
    pp.TokenType.T_MINUS: operator.sub,
    pp.TokenType.T_MULT: operator.mul,
    pp.TokenType.T_DIV: operator.truediv
}


def compute(node):
    if node.token_type == pp.TokenType.T_NUM:
        return node.value
    left_result = compute(node.children[0])
    right_result = compute(node.children[1])
    operation = operations[node.token_type]
    return operation(left_result, right_result)


if __name__ == '__main__':
    ast = pp.parse(sys.argv[1])
    result = compute(ast)
    print(result)
