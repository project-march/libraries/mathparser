import pytest

from mathparser import compute
from mathparser import parser as pp

def compute_compare_tool(inputstring, expected_output):
    ast = pp.parse(inputstring)
    actual_result = compute.compute(ast)
    print('{} should evaluate to {}, actual result is {}'.format(inputstring, expected_output, actual_result))
    assert actual_result == expected_output

def test_values():
    compute_compare_tool('1+1', 2)
    compute_compare_tool('1-1', 0)
    compute_compare_tool('3-2+1', 2)
    compute_compare_tool('8/4/2', 1)
    compute_compare_tool('1*2', 2)
    compute_compare_tool('7/4', 1.75)
    compute_compare_tool('2*3+4', 10)
    compute_compare_tool('2*(3+4)', 14)
    compute_compare_tool('2+3*4', 14)
    compute_compare_tool('2+(3*4)', 14)
    compute_compare_tool('2-(3*4+1)', -11)
    compute_compare_tool('2*(3*4+1)', 26)
    compute_compare_tool('80/((10+30)*20)', 0.1)

    try:
        compute_compare_tool('1+1)', 1)
        raised = False
    except ValueError:
        raised = True
    assert raised

def test_just_numbers():
    compute_compare_tool('20', 20)
    compute_compare_tool('100', 100)
    compute_compare_tool('10012', 10012)

def test_with_backets():
    compute_compare_tool('(1+7)*(9+2)', 88)
    compute_compare_tool('(2+7)/4', 2.25)

def test_with_negatives():
    compute_compare_tool('-1', -1)
    compute_compare_tool('(-20)*2', 40)

def test_with_decimals():
    compute_compare_tool('1.0', 1)
    compute_compare_tool('10.5/5.0', 2.1)