#!/usr/bin/env python

from distutils.core import setup

setup(name='mathparser',
      version='1.0',
      description='Fork of parser: A Math Parser for Python',
      author='Georg Nebehay',
      url='https://gitlab.com/project-march/libraries/mathparser/',
      packages=['mathparser'],
     )